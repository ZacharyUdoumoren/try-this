package ca.comp2633.zoju;

import java.lang.RuntimeException;

class ProgramDoesNotExistException extends RuntimeException {}
