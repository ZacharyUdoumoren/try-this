package ca.comp2633.zoju;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author OP
 */
public class NewAccRegistration extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        final GridPane accGrid = new GridPane();
        accGrid.setHgap(10);
        accGrid.setVgap(10);
        accGrid.setPadding(new Insets(25,25,25,25));
        Scene scene = new Scene(accGrid,400,300);
                primaryStage.setScene(scene);

        Text createText = new Text("Create XYZ University account");
        createText.setFont(Font.font("Tohama", FontWeight.NORMAL, 22));
        accGrid.add(createText, 0, 0, 2, 1);

        Label newUserNameLabel = new Label("New XYZ ID:");
        accGrid.add(newUserNameLabel,0,1);

        TextField newUserNameField = new TextField();

        accGrid.add(newUserNameField,1,1);

        Label newPwLabel = new Label("Set new password:");
        Label confirmNewPwLabel = new Label("Confirm new password:");

        final PasswordField newPwField = new PasswordField();
        final PasswordField confirmPwField = new PasswordField();

        accGrid.add(newPwLabel,0,2);
        accGrid.add(confirmNewPwLabel,0,3);


        accGrid.add(newPwField,1,2);
        accGrid.add(confirmPwField,1,3);


        Button createBtn = new Button("Create");
        accGrid.add(createBtn, 0, 4);

        createBtn.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                if(newPwField.getText().equals(confirmPwField.getText())){
                    System.out.println("New account will be created.");
                }else{
                    Text passWordNotMatch = new Text("Passwords do not match!");
                    passWordNotMatch.setFill(Color.RED);
                    accGrid.add(passWordNotMatch, 0, 6);
                }


            }




        });

















        primaryStage.show();

    }



}
