package ca.comp2633.zoju;

import org.json.*;

class Contact extends JSONObject {

    public Contact( JSONObject json_obj ) {
        super(json_obj.toString());
    }

    public String getEmail() {
        return this.getString("email");
    }

    public String getDirector() {
        return this.getString("director");
    }

    public String getPhone() {
        return this.getString("phone");
    }

    public String getLocation() {
        return this.getString("location");
    }

    public void getEmail(String email) {
        this.put("email", email);
    }

    public void getDirector(String director) {
        this.put("director", director);
    }

    public void getPhone(String phone) {
        this.put("phone", phone);
    }

    public void getLocation(String location) {
        this.put("location", location);
    }

}
