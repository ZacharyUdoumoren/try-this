package ca.comp2633.zoju;

import java.util.Iterator;
import org.json.*;

public class Requirements extends JSONObject {

    private JSONObject and;
    private JSONObject or;

    public Requirements( JSONObject json_obj ) {
        super(json_obj.toString());
        this.and = this.getJSONObject("AND");
        this.or = this.getJSONObject("OR");
    }

    public double getCompetitiveAvrgMin() {
        return getDouble("competitive_average_min");
    }

    public double getCompetitiveAvrgMax() {
        return getDouble("competitive_average_max");
    }

    public double getMinimumFinalAvrg() {
        return getDouble("minimum_final_average");
    }

    @Override
    public String toString () {
        String requirements = "";
        Iterator<String> keys = and.keys();
        while(keys.hasNext()) {
            String key = keys.next();
            requirements += key;
            requirements += " ";
            requirements += and.getString(key);
            requirements += "%";
            requirements += "\n";
        }

        keys = or.keys();
        if(keys.hasNext()) {
            requirements += "One of: \n";
        }
        while(keys.hasNext()) {
            String key = keys.next();
            requirements += key;
            requirements += " ";
            requirements += or.getString(key);
            requirements += "%";
            if(keys.hasNext()) {
                requirements += " or";
            }
            requirements += "\n";
        }

        return requirements;
    }
}
