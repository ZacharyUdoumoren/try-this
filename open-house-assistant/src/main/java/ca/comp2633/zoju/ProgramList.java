package ca.comp2633.zoju;

import java.util.Iterator;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONObject;

class ProgramList extends Hashtable<Integer,Program> {

    public ProgramList(String JSONList) {
        JSONObject obj = new JSONObject(JSONList);
        Iterator<String> keys = obj.keys();

        //Populate List
        while(keys.hasNext()) {
            String key_string = keys.next();
            int key = Integer.parseInt(key_string);
            this.put(key,new Program(obj.getJSONObject(key_string)));
        }

    }

    public LinkedList getSortedByName(){
       //Transfer as List and sort it
        LinkedList<Program> list = new LinkedList(this.values());

        Collections.sort(list, new Comparator<Program>(){
               public int compare(Program p1, Program p2) {
                   return p1.getName().compareTo(p2.getName());
               }});

       return list;
    }

    public LinkedList getSortedByDepartment(){
       //Transfer as List and sort it
        LinkedList<Program> list = new LinkedList(this.values());

        Collections.sort(list, new Comparator<Program>(){
               public int compare(Program p1, Program p2) {
                   int deptDif = p1.getDepartment().compareTo(p2.getDepartment());
                   int nameDif = p1.getName().compareTo(p2.getName());
                   return (deptDif == 0) ? nameDif : deptDif;
               }});

       return list;
    }

    public int getID(Program program)
        throws ProgramDoesNotExistException {
        int id = 0;
        boolean found = false;
        for (int i : this.keySet()) {
            if (this.get(i).equals(program)) {
                found = true;
                id = i;
            }
        }

        if(!found) {
            throw new ProgramDoesNotExistException();
        }
        return id;
    }

    public Program getProgram( int key) throws ProgramDoesNotExistException {
        Program program;
        if ( this.containsKey(key) ) {
            program = get(key);
        } else {
            throw new ProgramDoesNotExistException();
        }
        return program;
    }

    public String getName( int key ) {
        String name;
        if ( this.containsKey(key)) {
            name = get(key).getName();
        } else {
            name = "Empty";
        }
        return name;
    }
}
