package ca.comp2633.zoju;

import java.util.Iterator;
import org.json.*;

class Program extends JSONObject {
    private Requirements requirements;
    private Contact contact;
    private InfoSessions info_sessions;

    public Program( JSONObject json_obj ) {
        super(json_obj.toString());
        requirements = new Requirements(getJSONObject("requirements"));
        contact = new Contact(getJSONObject("contact"));
        info_sessions = new InfoSessions(getJSONObject("info_sessions"));
    }

    public String getName() {
        return getString("name");
    }

    public String getDepartment() {
        return getString("department");
    }

    public String getDescription() {
        return getString("description");
    }

    public Requirements getRequirements() {
        return requirements;
    }

    public InfoSessions getInfoSessions() {
        return info_sessions;
    }

    public Contact getContact() {
        return contact;
    }

}
