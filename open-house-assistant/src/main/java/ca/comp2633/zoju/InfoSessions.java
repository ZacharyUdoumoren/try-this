package ca.comp2633.zoju;

import java.util.*;

import org.json.*;

class InfoSessions extends Hashtable<String,InfoSession> {
    public InfoSessions (JSONObject json_obj) {
        Iterator<String> keys = json_obj.keys();

        while(keys.hasNext()) {
            String key = keys.next();
            this.put(key,new InfoSession(json_obj.getJSONObject(key)));
        }
    }

    public InfoSession getInfoSession(String key)
        throws InfoSessionDoesNotExistException {
        InfoSession info_session;
        if( this.containsKey(key) ) {
            info_session = get(key);
        } else {
            throw new InfoSessionDoesNotExistException();
        }
        return info_session;
    }
}
