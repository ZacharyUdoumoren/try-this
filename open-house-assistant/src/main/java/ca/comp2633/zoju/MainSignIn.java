package ca.comp2633.zoju;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

/**
 *
 * @author Olga Purdik
 */
public class MainSignIn extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        GridPane daGrid = new GridPane();
        daGrid.setAlignment(Pos.CENTER);
        daGrid.setHgap(10);
        daGrid.setVgap(10);
        daGrid.setPadding(new Insets(25, 25, 25, 25));
        Scene scene = new Scene(daGrid, 387, 275);

        primaryStage.setScene(scene);
        Text welcomeText = new Text("University of XYZ Course Explorer");
        welcomeText.setFont(Font.font("Tohama", FontWeight.NORMAL, 22));
        daGrid.add(welcomeText, 0, 0, 2, 1);

        Label userNameLabel = new Label("XYZ ID:");

        daGrid.add(userNameLabel, 0, 1);

        final TextField userNameField = new TextField();

        daGrid.add(userNameField, 1, 1);

        Label pwLabel = new Label("password:");

        daGrid.add(pwLabel, 0, 2);

        final PasswordField pwField = new PasswordField();

        daGrid.add(pwField, 1, 2);

        Button signInBtn = new Button("Sign in");

        signInBtn.setAlignment(Pos.BOTTOM_RIGHT);

        daGrid.add(signInBtn, 1, 6);

        Hyperlink mkNewAcc = new Hyperlink("Click here");
        TextFlow flow = new TextFlow(
                new Text("Don't have a XYZ ID? "), mkNewAcc
        );

        flow.setTextAlignment(TextAlignment.RIGHT);
        daGrid.add(flow, 2, 6);

        signInBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                System.out.println("signing in with username: " + userNameField.getText() + " and Password: " + pwField.getText());
            }

        });

        mkNewAcc.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent event) {
                System.out.println("making new account");
                NewAccRegistration acc = new NewAccRegistration();
                try {
                    acc.start(primaryStage);
                } catch (Exception ex) {
                    Logger.getLogger(MainSignIn.class.getName()).log(Level.SEVERE, null, ex);
                }
            }




        });



        primaryStage.show();

    }

}
