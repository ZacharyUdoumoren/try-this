package ca.comp2633.zoju;

import org.json.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.util.Locale;

class InfoSession extends JSONObject {
    public InfoSession(JSONObject json_obj) {
        super(json_obj.toString());
    }

    public String getTimeString() {
        return this.getString("time");
    }

    public String getDateString() {
        return this.getString("date");
    }

    public String getLocation() {
        return this.getString("location");
    }

    public LocalDate getDate() {
        String format = getDateString() + getTimeString();
        DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("MM/dd/yyh:ma", Locale.ENGLISH);
        LocalDate date = LocalDate.parse(format, formatter);
        return date;
    }
}
