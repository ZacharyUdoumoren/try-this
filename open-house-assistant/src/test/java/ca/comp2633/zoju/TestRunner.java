package ca.comp2633.zoju;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {

	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(AllTests.class);

		System.out.println("All tests were successful? " + result.wasSuccessful());

		if(result.getFailures().size()>0){
			for (Failure failure : result.getFailures()) {
				System.out.println(failure.toString());
			}
			System.out.println("Failure Exist");
			System.exit(1);
		}
	}
}
