package ca.comp2633.zoju;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.io.File;
import java.util.Scanner;
import java.util.*;


public class TestProgramList  {

    private static final String programData = "data/programData.json";
    private ProgramList programs;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
        try {
            Scanner scanner = new Scanner( new File(programData) );
            String content = scanner.useDelimiter("\\A").next();
            scanner.close();

            ProgramList programs = new ProgramList(content);

        } catch (Exception e) {
            e.printStackTrace();
        }

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddPositive() throws Exception {
        assertTrue(true);
	}

}
